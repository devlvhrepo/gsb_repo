<br/>
<form action="index.php?uc=validerFicheFrais&action=validerFrais" method="POST">
    <div id="corpsForm">
        <fieldset>
            <legend><h3><?php echo("$prenom $nom : $numMois/$numAnnee");?></h3></legend>
            <br/>
            <fieldset>
                <legend>R&eacute;capitulatif : </legend>
                 Etat : <?php echo $libEtat?> depuis le <?php echo $dateModif?> <br> Montant validé : <?php echo $montantValide?>
            </fieldset>
            <fieldset>
                <legend>Frais forfaitis&eacute;s : </legend>
                <?php
                    if ($lesFraisForfait ==NULL){
                        ?>
                <p>
                    Aucuns frais forfaitis&eacute; pour la p&eacute;riode choisie.
                </p>
                        <?php
                    }else{
                        foreach ($lesFraisForfait as $unFrais)
                        {
                                $idFrais = $unFrais['idfrais'];
                                $libelle = $unFrais['libelle'];
                                $quantite = $unFrais['quantite'];
                        ?>
                            <p>
                                    <label for="idFrais" ><?php echo $libelle ?></label>
                                    <input type="text" id="idFrais" name="lesFrais[<?php echo $idFrais?>]" size="10" maxlength="5" value="<?php echo $quantite?>" >
                            </p>

                        <?php
                        }
                    }
                        ?>
            </fieldset>
            <br/>
            <fieldset>
                <legend>Frais hors forfait : </legend>
                    <?php
                        if ($lesFraisHorsForfait == NULL){
                            ?>
                <p>
                    Aucuns frais hors forfait pour la p&eacute;riode s&eacute;lectionn&eacute;e.
                </p>
                            <?php
                        }else{
                    ?>
                                <table class="listeLegere">
                                    <tr>
                                       <th class="date">Date</th>
                                       <th class="libelle">Libellé</th>  
                                       <th class="montant">Montant</th>  
                                       <th class="action">Etat frais</th>              
                                    </tr>

                                    <?php
                                        
                                        foreach( $lesFraisHorsForfait as $unFraisHorsForfait) 
                                            {
                                                
                                                $libelle = $unFraisHorsForfait['libelle'];
                                                $date = $unFraisHorsForfait['date'];
                                                $montant=$unFraisHorsForfait['montant'];
                                                $id = $unFraisHorsForfait['id'];
                                                $etat = $unFraisHorsForfait['etat']
                                    ?>		
                                    <tr>
                                        <td> <?php echo $date ?></td>
                                        <td><?php echo $libelle ?></td>
                                        <td><?php echo $montant ?></td>
                                        <td>
                                            <?php if($etat != "NTR"){?>
                                                <select size="3" name="fraisHF[<?php echo $id?>]" >
                                                    <option value="REF" selected>Refuser</option>
                                            <?php }else{ ?>
                                                <select size="3" name="fraisHF[<?php echo $id?>]">    
                                                    <option value="VAL">Valider</option>
                                                    <option value="REF">Refuser</option>
                                                    <option value="REP">Reporter</option>
                                            <?php } ?>   
                                            </select>
                                        </td>
                                     </tr>
                                        <?php		 
                                            }
                                        ?>
                                </table>
               
                        <?php  
                            }
                        ?>
            </fieldset>
            
            <br/>
            
            <fieldset>
                <legend>
                    Nombre de justificatifs re&ccedil;us :
                </legend>
                <p>
                    <input type="text" name="nbJustificatifs" value="<?php echo $nbJustificatifs;?>">
                </p>
            </fieldset>
           
            <fieldset>
                <input type="submit" value="Valider la fiche">
                <input type="reset" value="Annuler les modifications">
            </fieldset>
        </fieldset>
    </div>
    
</form>