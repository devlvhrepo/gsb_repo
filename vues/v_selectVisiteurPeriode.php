<form method="POST"  action="index.php?uc=validerFicheFrais&action=voirEtatFrais">
     <div class="corpsForm">
        <fieldset>
            <legend>
                Visiteur et p&eacute;riode : 
            </legend>
             <select	name="visiteurSel">
                 <option value="default">
                    Selectionner un visiteur...
                </option>
                    <?php
                            foreach ($listVisiteur as $visiteur)
                            {
                                $idVisiteur = $visiteur['id'];
                                $nom = $visiteur['nom'];
                                $prenom = $visiteur['prenom'];
                                
                                if (isset($_REQUEST['visiteurSel'])&& $_REQUEST['visiteurSel']==$idVisiteur){
                                    //option + selected
                                    ?>
                                    <option value="<?php echo($idVisiteur);?>" selected>
                                        <?php echo("$nom $prenom");?>
                                    </option>
                                                                   
                                <?php 
                                }else{
                                    ?>                      
                                    <option value="<?php echo($idVisiteur);?>">
                                        <?php echo("$nom $prenom");?>
                                    </option>
                                    <?php
                                }
                            }    
                    ?>
                    
            </select>
            <select name="periodeSel">
                <option value="default">
                    Selectionner un visiteur...
                </option>
                    <?php
                        foreach ($listPeriode as $periode){
                            $idPeriode = $periode['mois'];
                            $numMois = $periode['numMois'];
                            $numAnnee = $periode['numAnnee'];

                            if (isset($_REQUEST['periodeSel'])&& $_REQUEST['periodeSel']==$idPeriode){
                                
                                ?>
                                <option value="<?php echo($idPeriode);?>" selected>
                                    <?php echo("$numMois-$numAnnee");?>
                                </option>

                            <?php 
                            }else{
                                ?>                      
                                <option value="<?php echo $idPeriode;?>">
                                    <?php echo("$numMois-$numAnnee");?>
                                </option>
                                <?php
                            }
                        }    
                    ?>
                    
             </select>
            
            
            <input type="submit" value="valider">
        </fieldset>
     </div>
</form>
