
<fieldset>
    <legend>
        Fiches de frais mises en paiement : 
    </legend>
    <?php
    if ($listeFiches!= NULL){ ?>
    <table class="listeLegere">
        <tr>
           <th class="date">P&eacute;riode</th>
           <th class="libelle">Visiteur</th>  
           <th class="montant">Montant</th>  
           <th class="action"> </th>              
        </tr>
    <?php
        //Gérer ici l'affichage de la liste des fiches de frais dont le remboursement doit être confirmé sous forme de tableau
        // Mois | Visiteur | Montant | Détail
        //lien dans détail pour acceder à v_detailRemboursement.php (passer ne paramètre url l'id et le mois ? ou continuer en $_session)
        //Trier par date croissante, pour gérer les plus anciennes en premier.

        foreach ($listeFiches as $fiches){
            $numAnnee =substr($fiches['mois'],0,4);
            $numMois =substr($fiches['mois'],4,2);
            $nom = $fiches['nom'];
            $prenom = $fiches['prenom'];
            ?>
        <tr>
            <td><?php echo "$numMois / $numAnnee";?></td>
            <td><?php echo "$prenom $nom" ; ?></td>
            <td><?php echo $fiches['montant'] ;?></td>
            <td><a href="index.php?uc=paiementFrais&action=afficherRemboursement&mois=<?php echo $fiches['mois'];?>&id=<?php echo $fiches['id'];?>"> D&eacute;tail</a></td>
            
        </tr>
            <?php
        }

    ?>
    </table>
<?php }else{?>
    <p>
        Aucunes fiches de frais &agrave; rembourser.
    </p>
<?php } ?>
</fieldset>
