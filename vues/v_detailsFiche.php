<br/>
<form action="index.php?uc=paiementFrais&action=confirmerRemboursement" method="POST">
    <div id="corpsForm">
        <fieldset>
            <legend><h3><?php echo("$prenom $nom : $numMois/$numAnnee");?></h3></legend>
            <br/>
            <fieldset>
                <legend>R&eacute;capitulatif : </legend>
                 Etat : <?php echo $libEtat?> depuis le <?php echo $dateModif?> <br> Montant validé : <?php echo $montantValide?>
            </fieldset>
            <br/>
            <fieldset>
                <legend>Frais forfaitis&eacute;s : </legend>
                <?php
                    if ($lesFraisForfait ==NULL){
                        ?>
                <p>
                    Aucuns frais forfaitis&eacute; pour la p&eacute;riode choisie.
                </p>
                        <?php
                    }else{
                        foreach ($lesFraisForfait as $unFrais)
                        {
                                $idFrais = $unFrais['idfrais'];
                                $libelle = $unFrais['libelle'];
                                $quantite = $unFrais['quantite'];
                        ?>
                            <p>
                                    <?php echo "$libelle : $quantite"?>
                            </p>

                        <?php
                        }
                    }
                        ?>
            </fieldset>
            <br/>
            <fieldset>
                <legend>Frais hors forfait : </legend>
                    <?php
                        if ($lesFraisHorsForfait == NULL){
                            ?>
                <p>
                    Aucuns frais hors forfait pour la p&eacute;riode s&eacute;lectionn&eacute;e.
                </p>
                            <?php
                        }else{
                    ?>
                                <table class="listeLegere">
                                    <tr>
                                       <th class="date">Date</th>
                                       <th class="libelle">Libellé</th>  
                                       <th class="montant">Montant</th>                
                                    </tr>

                                    <?php
                                        
                                        foreach( $lesFraisHorsForfait as $unFraisHorsForfait) 
                                            {
                                                
                                                $libelle = $unFraisHorsForfait['libelle'];
                                                $date = $unFraisHorsForfait['date'];
                                                $montant=$unFraisHorsForfait['montant'];
                                                $id = $unFraisHorsForfait['id'];
                                                $etat = $unFraisHorsForfait['etat']
                                    ?>		
                                    <tr>
                                        <td> <?php echo $date ?></td>
                                        <td><?php echo $libelle ?></td>
                                        <td><?php echo $montant ?></td>
                                     </tr>
                                        <?php		 
                                            }
                                        ?>
                                </table>
               
                        <?php  
                            }
                        ?>
            </fieldset>
            
            <br/>
            
            <fieldset>
                <legend>
                    Nombre de justificatifs re&ccedil;us :
                </legend>
                <p>
                    <?php echo $nbJustificatifs;?>
                </p>
            </fieldset>
           
            <fieldset>
                <input type="submit" value="D&eacute;clarer le remboursement">
            </fieldset>
        </fieldset>
    </div>
    
</form>