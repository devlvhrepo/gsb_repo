<?php
include("vues/v_sommaire.php");


$action = $_REQUEST['action'];
switch($action){
    case 'selectRemboursement':{
        $listeFiches = $pdo->getFichesARembourser();
        include("vues/v_selectRemboursementFiche.php");
        break;
    }
    
    case 'afficherRemboursement':{
        //récupération de l'id de la fiche de frais à afficher en détail :
        $idVisiteur=  $_REQUEST['id'];
        $_SESSION['visiteurSel'] =  $idVisiteur;
        $leMois = $_REQUEST['mois'];
        $_SESSION['periodeSel'] = $leMois;
        $visiteur = $pdo->getNomVisiteur($idVisiteur);
        $prenom = $visiteur['prenom'];
        $nom = $visiteur['nom'];
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur,$leMois);
        $lesFraisForfait= $pdo->getLesFraisForfait($idVisiteur,$leMois);
        $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur,$leMois);
        $numAnnee =substr( $leMois,0,4);
        $numMois =substr( $leMois,4,2);
        $libEtat = $lesInfosFicheFrais['libEtat'];
        $montantValide = $lesInfosFicheFrais['montantValide'];
        $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
        $dateModif =  $lesInfosFicheFrais['dateModif'];
        $dateModif =  dateAnglaisVersFrancais($dateModif);
        include("vues/v_detailsFiche.php");
        //Affichage du détail de la fiche de frais
        //Confirmation formulaire index.php?uc=paiementFiche&action=confirmerRemboursement

        break;
    }
    case 'confirmerRemboursement':{
        //Récupération de l'id de la fiche de frais
        $idVisiteur = $_SESSION['visiteurSel'];
        $mois = $_SESSION['periodeSel'];
        
        //Modification de l'état de la fiche pour RB - Remboursée
        $pdo->majEtatFicheFrais($idVisiteur, $mois, 'RB');
        ajouterConfirm("Remboursement effectué");
        include("vues/v_confirmation.php");

        break;
    }
}
?>