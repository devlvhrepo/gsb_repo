﻿<?php
if(!isset($_REQUEST['action'])){
	$_REQUEST['action'] = 'demandeConnexion';
}
$action = $_REQUEST['action'];
switch($action){
	case 'demandeConnexion':{
		include("vues/v_connexion.php");
		break;
	}
	case 'valideConnexion':{
		$login = $_REQUEST['login'];
		$mdp = $_REQUEST['mdp'];
		$personnel = $pdo->getInfosVisiteur($login,$mdp);
                $nomPoste=$pdo->getTypePersonnel($login);
		if(!is_array( $personnel)){
			ajouterErreur("Login ou mot de passe incorrect");
			include("vues/v_erreurs.php");
			include("vues/v_connexion.php");
		}
		else{
                        $_SESSION['selection']=false;
                        $_SESSION['verifCloture']=false ;
			$id = $personnel['id'];
			$nom =  $personnel['nom'];
			$prenom = $personnel['prenom'];
                        $type = $personnel['type'];
                        $libelle = $nomPoste['libelle'];
			connecter($id,$nom,$prenom,$type,$libelle);
			include("vues/v_sommaire.php");
		}
		break;
	}
	default :{
		include("vues/v_connexion.php");
		break;
	}
}
?>