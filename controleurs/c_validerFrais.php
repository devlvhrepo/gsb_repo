<?php
//Test pour ne pas réexecuter le requête de cloture à chaque action sur le site.
if(!isset($_SESSION['verifCloture'])){
    $_SESSION['verifCloture']=false;
}
//Cloture automatique de toutes les fiches de frais du mois précendent, après le 10 du mois suivant.
if (getNumJour()>9 && !$_SESSION['verifCloture']){
    include("controleurs/c_clotureFiches.php");
}
$listVisiteur=$pdo->getListeVisiteurs();
$listPeriode =$pdo->getPeriodesDisponibles();



$action = $_REQUEST['action'];
switch ($action){
    case 'selectVisiteur':{
        if(isset($_SESSION['visiteurSel'])){
            unset($_SESSION['visiteurSel']);
        }
        if(isset($_SESSION['periodeSel'])){
            unset($_SESSION['periodeSel']);
        }
        include("vues/v_sommaire.php");
        include('vues/v_selectVisiteurPeriode.php');
        break;
    }
    case 'voirEtatFrais':{
        //Récupération des infos pour l'affichage : 
        include("vues/v_sommaire.php");
        if(!isset($_SESSION['periodeSel'])){
            $_SESSION['periodeSel'] = $_REQUEST['periodeSel'];
        }
        if(!isset($_SESSION['visiteurSel'])){
            $_SESSION['visiteurSel'] = $_REQUEST['visiteurSel'];
        }
        $visiteur = $pdo->getNomVisiteur($_SESSION['visiteurSel']);
        $prenom = $visiteur['prenom'];
        $nom = $visiteur['nom'];
        $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($_SESSION['visiteurSel'], $_SESSION['periodeSel']);
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($_SESSION['visiteurSel'],$_SESSION['periodeSel']);
        $lesFraisForfait= $pdo->getLesFraisForfait($_SESSION['visiteurSel'],$_SESSION['periodeSel']);
        $numAnnee =substr( $_SESSION['periodeSel'],0,4);
        $numMois =substr( $_SESSION['periodeSel'],4,2);
        $libEtat = $lesInfosFicheFrais['libEtat'];
        $montantValide = $lesInfosFicheFrais['montantValide'];
        $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
        $dateModif =  $lesInfosFicheFrais['dateModif'];
        $dateModif =  dateAnglaisVersFrancais($dateModif);
        
        //Affichge :
        if ($_SESSION['visiteurSel']=='default'){
            ajouterErreur('Aucun visiteur sélectionné');
        }
        if ($_SESSION['periodeSel'] =='default'){
        ajouterErreur('Aucune période sélectionnée');
        }
        if (nbErreurs()!=0){
            include("vues/v_erreurs.php");
        }else if ($lesInfosFicheFrais['idEtat']!='CL'){
            include("vues/v_information.php");
        }else{
        include("vues/v_validationFrais.php");
        }
        break;
    }
    
    
    
    case 'validerFrais':{
        //Variables pour comparer le nombre d'action HF selectionnées et le nombre de frais HF existants pour la période.
        $nbFraisHF = $pdo->countFraisHorsForfait($_SESSION['visiteurSel'], $_SESSION['periodeSel']);
        if(isset($_REQUEST['fraisHF'])){
            $nbActionsHF = count($_REQUEST['fraisHF']);
            $idFraisHF= $pdo->getLesFraisHorsForfait($_SESSION['visiteurSel'],$_SESSION['periodeSel']);
        }else{
            $nbActionsHF=0;
        }
        
        $nbJustificatifs = $_REQUEST['nbJustificatifs'];
        
        //Gestion du statut des frais hors forfait :
        //Selection obligatoire pour l'action liée au frais concerné
        if ($nbFraisHF != $nbActionsHF){
            ajouterErreur("Vous devez sélectionner une action pour chaque frais hors forfait");
        }else
            
        //Gestion de la mise à jour du frais
            if($nbFraisHF!=0){
            $fraisHF = $_REQUEST['fraisHF'];
            for ($i=0; $i<$nbFraisHF; $i++){
                $idTemp = $idFraisHF[$i];
                $id = $idTemp[0];
                
                //Frais refusé : 
                if ($fraisHF[$id]=='REF'){
                    $mois = $_SESSION['periodeSel'];
                    $etat='REF';
                    $etatTemp = $pdo->etatFraisHF($id);
                    //Controle pour ne pas reporter indéfiniment les frais refusés
                    if ($etatTemp['etat']=='NTR'){
                        if($pdo->estPremierFraisMois($_SESSION['visiteurSel'], $mois+1)){
                            $pdo->creeNouvellesLignesFrais($_SESSION['visiteurSel'],$mois+1);
                        }
                        $pdo->majEtatFraisHorsForfait($id, $etat);
                        $libelle = $pdo->getLibelleFraisHF($id);
                        $pdo->majLibelleHorsForfait($id, $libelle);
                        $pdo->reporterFraisHorsForfait($id, $mois+1);
                        ajouterConfirm("Le frais $libelle à été refusé.");
                    }
               
                //Frais validé : 
                }else if ($fraisHF[$id]=='VAL'){
                    $mois = $_SESSION['periodeSel'];
                    $etat='VAL';
                    $pdo->majEtatFraisHorsForfait($id, $etat);
                    $libelle = $pdo->getLibelleFraisHF($id);
                    ajouterConfirm("Le frais $libelle à été validé.");
                    
                //Frais reporté :     
                }else if ($fraisHF[$id]=='REP'){
                    //ajouter structure de controle de l'existence d'une fiche de frais pour le mois+1 du $id
                    $mois = $_SESSION['periodeSel']+1;
                    if($pdo->estPremierFraisMois($_SESSION['visiteurSel'], $mois)){
                        $pdo->creeNouvellesLignesFrais($_SESSION['visiteurSel'],$mois);
                    }
                    $pdo->reporterFraisHorsForfait($id, $mois);
                    $libelle = $pdo->getLibelleFraisHF($id);
                    ajouterConfirm("Le frais $libelle à été reporté au mois prochain.");
                }
            }
        }   
            
            //Mise à jour des frais forfaitisés :
            $lesFrais = $_REQUEST['lesFrais'];
		if(lesQteFraisValides($lesFrais)){
	  	 	$pdo->majFraisForfait($_SESSION['visiteurSel'], $_SESSION['periodeSel'],$lesFrais);
		}
            //Mise à jour de l'état de la fiche de frais  
            $pdo->majEtatFicheFrais($_SESSION['visiteurSel'], $_SESSION['periodeSel'], 'VA');
            ajouterConfirm("La fiche de frais passe en statut Validé.");
            
            //Mise à jour du nombre de justificatifs reçus
            $pdo->majNbJustificatifs($_SESSION['visiteurSel'], $_SESSION['periodeSel'],$nbJustificatifs);
            ajouterConfirm("Nombre de justificatifs reçus : $nbJustificatifs");
            
            //Calcul du montant validé et mise à jour de la fiche de frais
            $montantRbtForfait = $pdo->getMontantsForfait();
            $montantForfait= array();
            foreach ($montantRbtForfait as $forfait){
                $montantForfait[$forfait['id']]= $forfait['montant'];
            }
            $montantETP = $lesFrais['ETP']*$montantForfait['ETP'];
            $montantKM = $lesFrais['KM']*$montantForfait['KM'];
            $montantNUI = $lesFrais['NUI']*$montantForfait['NUI'];
            $montantREP = $lesFrais['REP']*$montantForfait['REP'];
            $montantHorsForfait = $pdo->getMontantHorsForfait($_SESSION['visiteurSel'], $_SESSION['periodeSel']);
            $montantValide = $montantETP + $montantKM+$montantNUI+$montantREP+$montantHorsForfait['total'] ;
            $pdo->majMontantValide($_SESSION['visiteurSel'], $_SESSION['periodeSel'], $montantValide);
            ajouterConfirm("Montant validé : $montantValide");
        }
        
        
        if (nbErreurs()!=0){
            include("vues/v_erreurs.php");
        }else{
            if (nbConfirm()!=0){
                include("vues/v_confirmation.php");
            }
        }
        include("vues/v_redirect.php");
        
        
    }

