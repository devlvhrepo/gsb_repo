<?php
require_once("../include/fct.inc.php");
require_once ("../include/class.pdogsb.inc.php");
require_once("../include/class.fpdf.inc.php");
require_once("../include/class.pdf.inc.php");

session_start();
$pdo = PdoGsb::getPdoGsb();


//Récupération d'information dans la BDD : 
    //Information sur le visiteur :
        $visiteur = $pdo->getNomVisiteur($_SESSION['idVisiteur']);
        $nom = $visiteur['prenom']." ".$visiteur['nom'] ;
    //Informations sur la période :
        $numAnnee =substr( $_REQUEST['mois'],0,4);
        $numMois =substr( $_REQUEST['mois'],4,2);
        $mois = "".$numMois."/".$numAnnee."" ;
    //Information sur la fiche de frais : 
        $ficheFrais = $pdo->getLesInfosFicheFrais($_SESSION['idVisiteur'],$_REQUEST['mois']);
    //Informations sur les frais forfaitisés : 
        $lesFraisForfait = $pdo->getLesFraisForfait($_SESSION['idVisiteur'],$_REQUEST['mois'] );
    //Informations sur les frais hors forfait : 
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($_SESSION['idVisiteur'],$_REQUEST['mois']);

//Creation du document PDF
    $pdf = new PDF('P','mm','A4');
    $pdf->addPage();
    $pdf->SetFont('Helvetica','',11);
    $pdf->SetTextColor(0);

//Insertion identité visiteur : 
    $positionY=70; //Positionnement en dessous du header 
    $pdf->infoFicheFrais($nom, $mois);
    
//Frais Forfaitisés : 
    $pdf->enTeteTableauForfait($positionY);
    $positionY += 16 ; //Positionnement en dessous de l'en-tête du tableau
    //Boucle d'insertion des frais forfait : 
    $sousTotalFraisForfait = 0 ;
    foreach($lesFraisForfait as $unFrais){
        $libelle = $unFrais['libelle'];
        $quantite = $unFrais['quantite'];
        $montantUnitaire = $unFrais['montant'];
        $total = $quantite * $montantUnitaire ;
        $sousTotalFraisForfait+=$total ;
        $pdf->ligneTableauForfait($libelle, $quantite, $montantUnitaire, $total, $positionY);
        $positionY+=8;
    }
    //Sous Total : 
    $pdf->sousTotal($sousTotalFraisForfait, $positionY);
    $positionY+=16 ; //Décalage vers le bas pour positionnement des frais hors forfait 
    
//Frais Hors Forfait : 
    $pdf->enTeteTableauHorsForfait($positionY);
    $positionY+=16; 
    $sousTotalFraisHorsForfait = 0 ;
    //Boucle d'insertion des frais hors forfait : 
    foreach ($lesFraisHorsForfait as $unFraisHF){
        //Test de la validité des lignes de frais hors forfait : 
        if ($unFraisHF['etat']=='VAL'){
            $date = $unFraisHF['date'];
            $libelle = $unFraisHF['libelle'];
            $montant = $unFraisHF['montant'];
            $sousTotalFraisHorsForfait+=$montant;
            $pdf->ligneFraisHorsForfait($date, $libelle, $montant, $positionY);
            $positionY+=8;
        }
    }
    //Sous total : 
    $pdf->sousTotal($sousTotalFraisHorsForfait, $positionY);
    $positionY+=16;
    
//Total et signature : 
    //Total de la fiche :
    $montantRembourse = $sousTotalFraisForfait + $sousTotalFraisHorsForfait ;
    $pdf->totalRembourse($montantRembourse, $mois, $positionY);
    $positionY+=16;
    //Signature : 
    $pdf->signer(dateAnglaisVersFrancais($ficheFrais['dateModif']), $positionY);
    
//Envoi du fichier au navigateur : 
    $pdf->Output("Fiche de frais - $nom - $mois.pdf", 'I');






?>
