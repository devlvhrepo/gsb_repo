<?php

class PDF extends FPDF{
    
    /*
     * Fonction qui crée le header du pdf avec le logo GSB
     */
    function Header(){
        $this->Image("../images/logo.jpg", 90,5,40);
        $this->Ln(25);
    }
   
    /*
     * Fonction qui crée la page, défini le bloc identifiant de la fiche de frais
     */
    function infoFicheFrais($nom, $mois){
        $this->Cell(190, 10, "REMBOURSEMENT DE FRAIS ENGAGES", 1, 1, 'C');
        $this->SetX(15);
        $this->Cell(60, 10, "Visiteur : ", 0, 0, 'L');
        $this->Cell(60, 10, $nom, 0, 0, 'L');
        $this->Cell(60, 10, "", 0, 1, 'R');
        $this->SetX(15);
        $this->Cell(60, 10, "Mois : ", 0, 0, 'L');
        $this->Cell(60, 10, $mois, 0, 0, 'L');
        $this->Cell(60, 10, "", 0, 1, 'R');
    }
    /////////////////////////////////////////////////////
    //FRAIS FORFAITISES
    /////////////////////////////////////////////////////
    
    /*
     * Fonction qui retourne l'en-tête du tableau des frais forfaitisés
     */
    function enTeteTableauForfait($positionY){
        $positionX = 15; 
        $this->SetY($positionY);
        $this->SetX($positionX);
        $this->MultiCell(180,8,utf8_decode("Frais Forfaitisés"),1,'C');
        $positionY+=8 ;
        $this->SetX($positionX);
        $this->MultiCell(45,8,  utf8_decode("Intitulé"),1,'L');
        $this->SetY($positionY);
        $positionX += 45 ;
        $this->SetX($positionX);
        $this->MultiCell(45,8,  utf8_decode("Quantité"),1,'R');
        $this->SetY($positionY);
        $positionX += 45 ;
        $this->SetX($positionX);
        $this->MultiCell(60,8,utf8_decode("Montant unitaire"),1,'R');
        $this->SetY($positionY);
        $positionX += 60 ;
        $this->SetX($positionX);
        $this->MultiCell(30,8,utf8_decode("Total"),1,'R');
    }
    
    /*
     * Fonction qui génère les lignes de frais forfaitisés
     */
    function ligneTableauForfait($libelle, $quantite,$montantUnitaire, $total, $positionY){
        $positionX = 15; 
        $this->SetY($positionY);
        $this->SetX($positionX);
        $this->MultiCell(45,8,utf8_decode($libelle),1,'L');
        $this->SetY($positionY);
        $positionX += 45 ;
        $this->SetX($positionX);
        $this->MultiCell(45,8,  utf8_decode($quantite),1,'R');
        $this->SetY($positionY);
        $positionX += 45;
        $this->SetX($positionX);
        $this->MultiCell(60,8,utf8_decode($montantUnitaire),1,'R');
        $this->SetY($positionY);
        $positionX += 60 ;
        $this->SetX($positionX);
        $this->MultiCell(30,8,utf8_decode($total),1,'R');
    }
    
    /*
     * Fonction qui ajoute un sous total à la partie frais forfaitisés
     */
    function sousTotal($montant, $positionY){
        $positionX = 105 ;
        $this->SetY($positionY);
        $this->SetX($positionX);
        $this->MultiCell(60,8,utf8_decode("Sous total : "),1,'R');
        $this->SetY($positionY);
        $positionX += 60 ;
        $this->SetX($positionX);
        $this->MultiCell(30,8,  utf8_decode($montant),1,'R');
    }
    
    ///////////////////////////////////////
    // FRAIS HORS FORFAIT : 
    ///////////////////////////////////////
    
    /*
     * Fonction qui crée l'en tête du tableau des frais hors forfait
     */
    function enTeteTableauHorsForfait($positionY){
        //Ligne frais hors forfait
        $positionX = 15; 
        $this->SetY($positionY);
        $this->SetX($positionX);
        $this->MultiCell(180,8,utf8_decode("Frais Hors Forfait"),1,'C');
        $positionY += 8;

        //ligne en tete hors forfait : 
        $this->SetY($positionY);
        $this->SetX($positionX);
        $this->MultiCell(45,8,utf8_decode("Date"),1,'L');
        $this->SetY($positionY);
        $positionX += 45 ;
        $this->SetX($positionX);
        $this->MultiCell(105,8,  utf8_decode("Description"),1,'L');
        $this->SetY($positionY);
        $positionX += 105 ;
        $this->SetX($positionX);
        $this->MultiCell(30,8,utf8_decode("Montant"),1,'R');
    }
  
    /*
     * Fonction qui gère l'affichage du détail des lignes de frais hors forfait
     */
    function ligneFraisHorsForfait($date, $libelle, $montant, $positionY){ 
        $positionX = 15; 
        $this->SetY($positionY);
        $this->SetX($positionX);
        $this->MultiCell(45,8,utf8_decode($date),1,'L');
        $this->SetY($positionY);
        $positionX += 45 ;
        $this->SetX($positionX);
        $this->MultiCell(105,8,  utf8_decode($libelle),1,'L');
        $this->SetY($positionY);
        $positionX += 105 ;
        $this->SetX($positionX);
        $this->MultiCell(30,8,utf8_decode($montant),1,'R');
    }
    
    //////////////////////////////////////
    // TOTAL ET SIGNATURE
    //////////////////////////////////////
    
    /*
     * Fonction qui crée le recap de la fiche :
     */
    function totalRembourse($montant,$mois, $positionY){
        $positionX = 105; 
        $this->SetY($positionY);
        $this->SetX($positionX);
        $this->MultiCell(60,8,utf8_decode("Total $mois"),1,'R');
        $positionX +=60;
        $this->SetY($positionY);
        $this->SetX($positionX);
        $this->MultiCell(30,8,utf8_decode($montant),1,'R');
    }
    
    /*
     * Fonction qui crée la signature alignée à droite
     * @param $mois date de modif sous la forme "7 janvier 2011"
     * @param $position ordonnée du haut du bloc signature
     */
    function signer($date, $positionY){
        $this->SetY($positionY);
        $this->Cell(180,6,utf8_decode("Fait à Paris, le ".$date."."),0,1,'R');
        $this->Cell(180,6,"Vu par l'agent comptable",0,1,'R');
        $positionY+=18 ;
        $this->Image("../images/signature.jpg", 140, $positionY, 60);
    }
}