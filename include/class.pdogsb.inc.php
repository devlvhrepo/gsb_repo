﻿<?php
/** 
 * Classe d'accès aux données. 
 
 * Utilise les services de la classe PDO
 * pour l'application GSB
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO 
 * $monPdoGsb qui contiendra l'unique instance de la classe
 
 * @package default
 * @author Cheri Bibi
 * @version    1.0
 * @link       http://www.php.net/manual/fr/book.pdo.php
 */

class PdoGsb{   		
      	private static $serveur='mysql:host=localhost';
      	private static $bdd='dbname=gsbV2';   		
      	private static $port='port=3301';   		
      	private static $user='root' ;    		
      	private static $mdp='' ;	
		private static $monPdo;
		private static $monPdoGsb=null;
/**
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */				
	private function __construct(){
    	PdoGsb::$monPdo = new PDO(PdoGsb::$serveur.';'.PdoGsb::$port.';'.PdoGsb::$bdd, PdoGsb::$user, PdoGsb::$mdp); 
		PdoGsb::$monPdo->query("SET CHARACTER SET utf8");
	}
	public function _destruct(){
		PdoGsb::$monPdo = null;
	}
/**
 * Fonction statique qui crée l'unique instance de la classe
 
 * Appel : $instancePdoGsb = PdoGsb::getPdoGsb();
 
 * @return l'unique objet de la classe PdoGsb
 */
	public  static function getPdoGsb(){
		if(PdoGsb::$monPdoGsb==null){
			PdoGsb::$monPdoGsb= new PdoGsb();
		}
		return PdoGsb::$monPdoGsb;  
	}
        

        
/**
 * Retourne les informations d'un visiteur
 
 * @param $login 
 * @param $mdp
 * @return l'id, le nom et le prénom sous la forme d'un tableau associatif 
*/
	public function getInfosVisiteur($login, $mdp){
		$req = "select personnel.id as id, personnel.nom as nom, personnel.prenom as prenom, personnel.type as type from personnel 
		where personnel.login='$login' and personnel.mdp='$mdp'";
		$rs = PdoGsb::$monPdo->query($req);
		$ligne = $rs->fetch();
		return $ligne;
	}
/**
 * Retourne les informations d'un visiteur
 
 * @param $login 
 * @param $mdp
 * @return l'id, le nom et le prénom sous la forme d'un tableau associatif 
*/
	public function getNomVisiteur($id){
		$req = "select personnel.id as id, personnel.nom as nom, personnel.prenom as prenom from personnel 
		where personnel.id='$id'";
		$rs = PdoGsb::$monPdo->query($req);
		$ligne = $rs->fetch();
		return $ligne;
	}
        
/**
 * Retourne la liste des visiteurs médicaux
 
 * @return l'id, le nom et le prénom sous la forme d'un tableau associatif 
*/
	public function getListeVisiteurs(){
		$req = "select personnel.id as id, personnel.nom as nom, personnel.prenom as prenom from personnel 
		where personnel.type='VST' ORDER BY personnel.nom ASC";
		$rs = PdoGsb::$monPdo->query($req);
		$ligne = $rs->fetchAll();
		return $ligne;
	}
        
        
/**
 * Retourne le libelle du type de poste d'un personnel.
 * 
 * @param $id du personnel voulu
 * @return type de poste sous forme de chaine de caracteres
 */        
        public function getTypePersonnel($login){
            $req = "select type.libelle from type where id in(select type from personnel where personnel.login='$login')" ;
            $rs = PdoGsb::$monPdo->query($req);
            $type = $rs->fetch();
            return $type ;
        }

/**
 * Retourne sous forme d'un tableau associatif toutes les lignes de frais hors forfait
 * concernées par les deux arguments
 
 * La boucle foreach ne peut être utilisée ici car on procède
 * à une modification de la structure itérée - transformation du champ date-
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @return tous les champs des lignes de frais hors forfait sous la forme d'un tableau associatif 
*/
	public function getLesFraisHorsForfait($idVisiteur,$mois){
	    $req = "select * from lignefraishorsforfait where lignefraishorsforfait.idvisiteur ='$idVisiteur' 
		and lignefraishorsforfait.mois = '$mois' ";	
		$res = PdoGsb::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		$nbLignes = count($lesLignes);
		for ($i=0; $i<$nbLignes; $i++){
			$date = $lesLignes[$i]['date'];
			$lesLignes[$i]['date'] =  dateAnglaisVersFrancais($date);
		}
		return $lesLignes; 
	}
/**
 * Retourne le nombre de justificatif d'un visiteur pour un mois donné
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @return le nombre entier de justificatifs 
*/
	public function getNbjustificatifs($idVisiteur, $mois){
		$req = "select fichefrais.nbjustificatifs as nb from  fichefrais where fichefrais.idvisiteur ='$idVisiteur' and fichefrais.mois = '$mois'";
		$res = PdoGsb::$monPdo->query($req);
		$laLigne = $res->fetch();
                if($laLigne['nb']==NULL){
                    $laLigne['nb'] = 0 ;
                }
		return $laLigne['nb'];
	}
        
/**
 * Fonction qui retourne le libelle  d'un frais hors forfait d'après un id de frais hors forfait.
 * @param type $id
 * @return le libelle du frais hors forfait
 */
        public function getLibelleFraisHF($id){
            $req = "SELECT libelle FROM lignefraishorsforfait where id=$id";
            $rs = PdoGsb::$monPdo->query($req);
            $libelleHF = $rs->fetch();
            return $libelleHF['libelle'] ;
            
        }
/**
 * Retourne sous forme d'un tableau associatif toutes les lignes de frais au forfait
 * concernées par les deux arguments
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @return l'id, le libelle et la quantité sous la forme d'un tableau associatif 
*/
	public function getLesFraisForfait($idVisiteur, $mois){
		$req = "select fraisforfait.id as idfrais, fraisforfait.libelle as libelle, fraisforfait.montant as montant,
		lignefraisforfait.quantite as quantite from lignefraisforfait inner join fraisforfait 
		on fraisforfait.id = lignefraisforfait.idfraisforfait
		where lignefraisforfait.idvisiteur ='$idVisiteur' and lignefraisforfait.mois='$mois' 
		order by lignefraisforfait.idfraisforfait";	
		$res = PdoGsb::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes; 
	}
        
        
/**
 * Fonction qui récupère le montant des différentes indemnités 
 * @return type le montant des indemnités sous forme d'un tableau associatif.
 */
        public function getMontantsForfait(){
            $req = "SELECT fraisforfait.id as id, fraisforfait.montant as montant FROM fraisforfait";
            $res = PdoGsb::$monPdo->query($req);
		$montants = $res->fetchAll();
            return $montants ;
        }
        
        
/**
 * Fonction qui retourne la somme des montants hors forfaits validés selon un id et un mois 
 * @param type $idVisiteur
 * @param type $mois sous forme aaaamm
 * @return type somme des frzis hors forfaits validés pour ce visiteur et ce mois
 */
        public function getMontantHorsForfait($idVisiteur, $mois){
            $req = "SELECT SUM(montant) as total from lignefraishorsforfait WHERE idVisiteur='$idVisiteur' AND mois='$mois' AND etat='VAL'";
            $res = PdoGsb::$monPdo->query($req);
            $montantHorsForfait = $res->fetch();
            return $montantHorsForfait;
        }
        
/*
 * Fonction qui retourne la liste des fiches de frais dont le statut est à VA - Validé et mis en paiement
 * @return $listeFiches, tableau associatif contenant id/nom/prenom/mois/montant des fiches de frais.
 */
        public function getFichesARembourser(){
            $req = "select personnel.id as id, personnel.nom as nom, personnel.prenom as prenom, fichefrais.mois as mois, fichefrais.montantValide as montant 
                    from personnel left join fichefrais on personnel.id = fichefrais.idVisiteur
                    WHERE fichefrais.idEtat = 'VA'
                    ORDER BY fichefrais.mois ASC";
            $rs=  PdoGsb::$monPdo->query($req);
            $listeFiches = $rs->fetchAll();
            return $listeFiches ;
        }
/**
 * Retourne tous les id de la table FraisForfait
 
 * @return un tableau associatif 
*/
	public function getLesIdFrais(){
		$req = "select fraisforfait.id as idfrais from fraisforfait order by fraisforfait.id";
		$res = PdoGsb::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}
/**
 * Met à jour la table ligneFraisForfait
 
 * Met à jour la table ligneFraisForfait pour un visiteur et
 * un mois donné en enregistrant les nouveaux montants
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @param $lesFrais tableau associatif de clé idFrais et de valeur la quantité pour ce frais
 * @return un tableau associatif 
*/
	public function majFraisForfait($idVisiteur, $mois, $lesFrais){
		$lesCles = array_keys($lesFrais);
		foreach($lesCles as $unIdFrais){
			$qte = $lesFrais[$unIdFrais];
			$req = "update lignefraisforfait set lignefraisforfait.quantite = $qte
			where lignefraisforfait.idvisiteur = '$idVisiteur' and lignefraisforfait.mois = '$mois'
			and lignefraisforfait.idfraisforfait = '$unIdFrais'";
			PdoGsb::$monPdo->exec($req);
		}
		
	}
/**
 * met à jour le nombre de justificatifs de la table ficheFrais
 * pour le mois et le visiteur concerné
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
*/
	public function majNbJustificatifs($idVisiteur, $mois, $nbJustificatifs){
		$req = "update fichefrais set nbjustificatifs = $nbJustificatifs 
		where fichefrais.idvisiteur = '$idVisiteur' and fichefrais.mois = '$mois'";
		PdoGsb::$monPdo->exec($req);	
	}
/**
 * Teste si un visiteur possède une fiche de frais pour le mois passé en argument
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @return vrai ou faux 
*/	
	public function estPremierFraisMois($idVisiteur,$mois)
	{
		$ok = false;
		$req = "select count(*) as nblignesfrais from fichefrais 
		where fichefrais.mois = '$mois' and fichefrais.idvisiteur = '$idVisiteur'";
		$res = PdoGsb::$monPdo->query($req);
		$laLigne = $res->fetch();
		if($laLigne['nblignesfrais'] == 0){
			$ok = true;
		}
		return $ok;
	}
/**
 * Retourne le dernier mois en cours d'un visiteur
 
 * @param $idVisiteur 
 * @return le mois sous la forme aaaamm
*/	
	public function dernierMoisSaisi($idVisiteur){
		$req = "select max(mois) as dernierMois from fichefrais where fichefrais.idvisiteur = '$idVisiteur'";
		$res = PdoGsb::$monPdo->query($req);
		$laLigne = $res->fetch();
		$dernierMois = $laLigne['dernierMois'];
		return $dernierMois;
	}
	
/**
 * Crée une nouvelle fiche de frais et les lignes de frais au forfait pour un visiteur et un mois donnés
 
 * récupère le dernier mois en cours de traitement, met à 'CL' son champs idEtat, crée une nouvelle fiche de frais
 * avec un idEtat à 'CR' et crée les lignes de frais forfait de quantités nulles 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
*/
	public function creeNouvellesLignesFrais($idVisiteur,$mois){
		$dernierMois = $this->dernierMoisSaisi($idVisiteur);
		$laDerniereFiche = $this->getLesInfosFicheFrais($idVisiteur,$dernierMois);
		if($laDerniereFiche['idEtat']=='CR'){
				$this->majEtatFicheFrais($idVisiteur, $dernierMois,'CL');
				
		}
		$req = "insert into fichefrais(idvisiteur,mois,nbJustificatifs,montantValide,dateModif,idEtat) 
		values('$idVisiteur','$mois',0,0,now(),'CR')";
		PdoGsb::$monPdo->exec($req);
		$lesIdFrais = $this->getLesIdFrais();
		foreach($lesIdFrais as $uneLigneIdFrais){
			$unIdFrais = $uneLigneIdFrais['idfrais'];
			$req = "insert into lignefraisforfait(idvisiteur,mois,idFraisForfait,quantite) 
			values('$idVisiteur','$mois','$unIdFrais',0)";
			PdoGsb::$monPdo->exec($req);
		 }
	}
/**
 * Crée un nouveau frais hors forfait pour un visiteur un mois donné
 * à partir des informations fournies en paramètre
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @param $libelle : le libelle du frais
 * @param $date : la date du frais au format français jj//mm/aaaa
 * @param $montant : le montant
*/
	public function creeNouveauFraisHorsForfait($idVisiteur,$mois,$libelle,$date,$montant){
		$dateFr = dateFrancaisVersAnglais($date);
		$req = "insert into lignefraishorsforfait (idVisiteur, mois, libelle, date, montant)
		values('$idVisiteur','$mois','$libelle','$dateFr','$montant')";
		PdoGsb::$monPdo->exec($req);
	}
/**
 * Supprime le frais hors forfait dont l'id est passé en argument
 
 * @param $idFrais 
*/
	public function supprimerFraisHorsForfait($idFrais){
		$req = "delete from lignefraishorsforfait where lignefraishorsforfait.id =$idFrais ";
		PdoGsb::$monPdo->exec($req);
	}
/**
 * Met à jour l'état d'un frais hors forfait refusé
 * @param type $idFrais 
 * @param type $libelle 
 * @param type $mois sous forme aaaamm
 */
        public function majEtatFraisHorsForfait($idFrais, $etat){
            $req = "UPDATE lignefraishorsforfait SET etat='$etat' WHERE id='$idFrais'";
            PdoGsb::$monPdo->exec($req);
        }
        
/**
 * Reporte un frais hors forfait au mois suivant
 * @param type $idFrais 
 * @param type $libelle 
 * @param type $mois sous forme aaaamm
 */
        public function reporterFraisHorsForfait($idFrais, $mois){
            $req = "UPDATE lignefraishorsforfait SET mois='$mois' WHERE id='$idFrais'";
            PdoGsb::$monPdo->exec($req);
        }
        
        public function majLibelleHorsForfait($id, $libelle){
            $libelleRefus = "REFUSE - $libelle";
            $req ="UPDATE lignefraishorsforfait SET libelle = '$libelleRefus' where id='$id'";
            PdoGsb::$monPdo->exec($req);
        }

        
        
/**
 * Retourne les mois pour lesquel un visiteur a une fiche de frais
 
 * @param $idVisiteur 
 * @return un tableau associatif de clé un mois -aaaamm- et de valeurs l'année et le mois correspondant 
*/
	public function getLesMoisDisponibles($idVisiteur){
		$req = "select fichefrais.mois as mois from  fichefrais where fichefrais.idvisiteur ='$idVisiteur' 
		order by fichefrais.mois desc ";
		$res = PdoGsb::$monPdo->query($req);
		$lesMois =array();
		$laLigne = $res->fetch();
		while($laLigne != null)	{
			$mois = $laLigne['mois'];
			$numAnnee =substr( $mois,0,4);
			$numMois =substr( $mois,4,2);
			$lesMois["$mois"]=array(
		     "mois"=>"$mois",
		    "numAnnee"  => "$numAnnee",
			"numMois"  => "$numMois"
             );
			$laLigne = $res->fetch(); 		
		}
		return $lesMois;
	}
        
/**
 * Fonction qui récupère les 12 dernières périodes disponibles
 * @return $lesMois un tableau associatif de clé un mois -aaaamm- et de valeurs l'année et le mois correspondant 
 */
	public function getPeriodesDisponibles(){
		$req = "SELECT mois FROM fichefrais GROUP BY mois ORDER BY mois DESC LIMIT 12";
		$res = PdoGsb::$monPdo->query($req);
		$lesMois =array();
		$laLigne = $res->fetch();
		while($laLigne != null)	{
			$mois = $laLigne['mois'];
			$numAnnee =substr( $mois,0,4);
			$numMois =substr( $mois,4,2);
			$lesMois["$mois"]=array(
		     "mois"=>"$mois",
		    "numAnnee"  => "$numAnnee",
			"numMois"  => "$numMois"
             );
			$laLigne = $res->fetch(); 		
		}
		return $lesMois;
	}
        
/**
 * Retourne les informations d'une fiche de frais d'un visiteur pour un mois donné
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @return un tableau avec des champs de jointure entre une fiche de frais et la ligne d'état 
*/	
	public function getLesInfosFicheFrais($idVisiteur,$mois){
		$req = "select ficheFrais.idEtat as idEtat, ficheFrais.dateModif as dateModif, ficheFrais.nbJustificatifs as nbJustificatifs, 
			ficheFrais.montantValide as montantValide, etat.libelle as libEtat from  fichefrais inner join Etat on ficheFrais.idEtat = Etat.id 
			where fichefrais.idvisiteur ='$idVisiteur' and fichefrais.mois = '$mois'";
		$res = PdoGsb::$monPdo->query($req);
		$laLigne = $res->fetch();
		return $laLigne;
	}
/**
 * Modifie l'état et la date de modification d'une fiche de frais
 
 * Modifie le champ idEtat et met la date de modif à aujourd'hui
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 */
 
	public function majEtatFicheFrais($idVisiteur,$mois,$etat){
		$req = "update ficheFrais set idEtat = '$etat', dateModif = now() 
		where fichefrais.idvisiteur ='$idVisiteur' and fichefrais.mois = '$mois'";
		PdoGsb::$monPdo->exec($req);
	}

        
        public function majMontantValide($idVisiteur, $mois, $montant){
            $req="UPDATE fichefrais SET montantValide='$montant' WHERE idVisiteur='$idVisiteur' AND mois='$mois'";
            PdoGsb::$monPdo->exec($req);
		
        }

/**
 * Fonction qui compte le nombre de lignefraishorsforfait pour un visiteur donné.
 * @param type $idVisiteur
 * @param type $periode
 * @return type nombre de frais hors forfait déclaré par un visiteur
 */
        public function countFraisHorsForfait($idVisiteur, $periode){
            $req = "select count(*) from lignefraishorsforfait where idVisiteur='$idVisiteur' and mois ='$periode' ";
            $rs = PdoGsb::$monPdo->query($req);
            $output=$rs->fetch();
            $result = $output[0];
            return $result ;
        }
        
/**
 * Fonction qui cloture les fiches de frais CR pour le(s) mois antérieur au mois courant
 * @param type $mois date au format aaaamm
 */
        public function clotureFichesFrais($mois){
            $req = "UPDATE  fichefrais SET idEtat='CL' WHERE mois < '$mois' AND idEtat='CR'";
            PdoGsb::$monPdo->exec($req);
        }
        
        
/**
 * Fonction qui retourne l'etat d'un frais hors forfait
 * @param int $id du frais hors forfait
 * @return String $result : chaine de caracteres contenant l'etat du frais hors forfait.
 */       
        public function etatFraisHF($id){
            $req = "SELECT etat FROM lignefraishorsforfait WHERE id='$id'";
            $rs = PdoGsb::$monPdo->query($req);
            $result=$rs->fetch();
            return $result ;
        }
        

/**
 * Fonction qui retourne un booléen indiquant si la fiche PDF peut être générée sur ce frais
 * @param type $idVisiteur
 * @param type $mois sous la forme AAAAMM
 * @return type
 */
        public function estPdfAble($idVisiteur, $mois){
            $req = "SELECT fichefrais.pdfable FROM fichefrais WHERE idVisiteur = '$idVisiteur' AND mois='$mois'";
            $rs = PdoGsb::$monPdo->query($req);
            $result=$rs->fetch();
            return $result['pdfable'] ;
        }
        
}
?>